// console.log("Hello World!");
// mini activity print your name in the console
// F12 or Ctrl + Shift + C to access the console
// console.log("Cee Doroteo");
// Ctrl / for comments

//Section: Syntax, Statements, and Comments

//Statement in programming are instructions that we tell the computer to perform
	//usually ends with semicolon (;)
	//train us to locate where a statement ends
	//alert("Hello");
	//console.log("Hi");

//Syntax in programming, it is the set of rules that describes how statements must be constructed
//All lines/blocks of code should be written in a specific manner to work.
//This is due to how these codes were initially programmed to function and perform in a certain manner

//Where to Place JavaScript
	// Inline - you can place JS right into the HTML page using the script tags
		//for very small sites and testing only
		//the inline approach does not scale well, leads to poor organization, and code duplication
	//External - this is a better approach
		//place JS into separate files and link to them from the HTML page
		//this approach is much easier to maintain, write and debug

//Use of Script Tag
	//script tag can go anywhere on the page
	//as a best practice, many developers will place it just before the closing body tag on the HTML page.
	//This provides daster speed load times for our web page

//we use devtools also to DEBUG, view messages and run JavaScript code in the console tab

		console.log("Hello");
//whitespace can impact functionality in many computer languages-BUT not in JavaScript. In JS, whitespace is used only for readability and has no functional impact. One effect of this is a single statement that can span multiple lines.

		console.log("Hello World1");

		console. log("Hello World2 " ) ;

		console.
		log
		(
			"Hello World3"
		);

//Comments
	//comments are parts of the code that gets ignored by the language
	//Comments are meant to describe the written code

		/*
			There are two types of comments:
			1. The single-line comment denoted by two slashes (ctrl /)
			2. The multi-line comment denoted by a slash and asterisk (ctrl shift /)
		*/

//Variables
	//it is used to contain data
	//any information that is used by our applications are storede in what we call a memory
	//when we create varaibles, certain portions of a device's memory is given a "name" that we call "variables"

	//this makes it easier for us to associate information stored in our dewices to actual "names" about information

		let x = 1;
		let y;

	//Declaring variables
		//tells our devices that a variable name is created and is ready to store data
		//declaring a variable without giving it a value will automatically assign it with the value of "undefined" meaning that the variable's value is "not defined"

	//Syntax
		//let/const variableName;
		//let/const variableNameOne;

	//let is a keyword that is usually used in declaring a variable

		let myVariable;

		//console.log() is useful for printing values of variables or certain results of code into the Google Chrome Browser's console

		console.log(myVariable);//undefined

		//console.log(hello);//

		//variables must be declared first before they are used
		//using variables before they are declared will return an error
		let hello;

		/*
			Guidelines in writing variables:
			1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value
			2. Variable names should start with a lowercase character, use camelCase for multiple words
			3. For Constant variables, use the 'const' keyword
			4. Variable names should be indicative or descriptove of the value being stored to avoid confusion

			Best practices in naming variables

			1.When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

			let firstName = "Michael";//good variable name
			let pokemon = "Charizard";//good variable name
			let pokemon = 25000;// bad variable name

			2.When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

			let FirstName = "Michael";//bad v name
			let lastName = "Jordan"; //good v name

			3.Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

			let first name = "Mickey"; //bad v name
			let firstName = "Mickey"; //good v name

			lastName emailAddress mobileNumber internetAllowance

			Underscores sample:

			let product_description = "cool product";


		*/

//Declaring and initializing variables
	//Initializing variables - the instance when a varibale is given its initial/starting value

	//Syntax
		//let/const variableName = value;

		let productName = 'desktop computer';
		console.log(productName);

		let productPrice = 30000;
		console.log(productPrice);

		// In the context of certain applications, some variables/information are constant and should not be changed
		// In this example, the interest rate for a loan, savings account or a mortgage must not be changed due to real world concerns
		// This is the best way to prevent applications from suddenly breaking or performing in ways that are not intended
		const interest = 3.539;

		//let- we usually use let if we want to reassign the value in our variable

		//Reassign variable values
		//means changing its initial or previous value into another value

		//Syntax
			//variableName = newValue;

		productName = 'Laptop';

		console.log(productName);

		//let friend = "Kate";
		//friend = 'Jane';

		let friend = "Kate";
		//let friend = "Jane";//error: friend has already been declared

		//interest = 4.4888;//error: const



		let supplier;//a. declaration

		supplier = "John Smith Tradings";//b. initialization
		console.log(supplier);

		supplier = "Zuitt Store";//c. reassignment
		console.log(supplier);


		//example:
		//const pi;
		//pi=3.1416;
		//console.log(pi);//error

		//let/const local/global scope

		//Scope means where these variables are available for use
		//let and const are block scoped
		//block is a chunk of code bounded by {}
		//a block lives in curly braces
		//anything within the curly braces is a block
		//so a variable declared in a block with let is only available for use within that block

		let outerVariable = 'hello';

		{
			let innerVariable = "hello again";
			console.log(innerVariable);
		}

		console.log(outerVariable);//hello
		//console.log(innerVariable);//undefined
	
//Multiple variable declarations
//multiple variables may be declared in one line
//though it is quicker to do without having to retype th let keyword

//let productCode = 'CD248', productBrand = "Dell";

	let productCode = 'CD248'; //best practice
	let productBrand = "Dell";	

console.log(productCode, productBrand);//CD248 Dell

//const let = "hello";
//console.log(let);//syntax error

//Section: Data Types
	//Strings
	//Strings are a series of chracters that create a word, a phrase, a sentence or anything related to creating text
	//Strings in JS can be written using either a single (') or (") quote
	//in other programming languages, only the double quotes can be used for creating strings
	let country = "Philippines";
	let province = "Metro Manila";

	//Concatenating strings
		//multiple string values can be combined to create a single string using the "+" symbol

		let fullAddress = province + ', ' + country;
			console.log(fullAddress);//Metro Manila, Philippines

		let greeting = 'I live in the ' + country + '.';
		console.log(greeting);//I live in the Philippines

		//the escape chracter (\) in strings in combination with other chracters can produce different effects

		// "\n" refers to creating a new line in between texts
		let mailAddress = 'Metro Manila\n\nPhilippines';
		console.log(mailAddress);

		let message = "John's employees went home early";
		console.log(message);//John's employees went home early
		message = 'John\'s employees went home early';
		console.log(message);//John's employees went home early

		//Numbers
		//Integers/Whole Numbers
		let headcount = 23;
		console.log(headcount);//23

		//Decimal Numbers/Fractions
		let grade = 98.7;
		console.log(grade);//98.7

		//exponential notation
		let planetDistance = 2e10;
		console.log(planetDistance);//20000000000

		//combine strings and numbers

		console.log("John's grade last month is " + grade);//John's grade last month is 98.7

		//Boolean
		//boolean values are normally used to store values relating to the state of certain things
		//this will be useful in further discussions about creating logic to make our application respond to certain scenarios
		let isMarried = false;
		let inGoodConduct = true;

		console.log("isMarried: " + isMarried);//isMarried: false
		console.log("inGoodConduct" + inGoodConduct);//inGoodConduct: true;

		//Arrays
		//Arrays are a special kind of data type that's used to store multiple values
		//Arrays can store diff data types but it is normally to similar data types

		//similar data types
		//syntax
			//let/const arrayName = [elementA, elementB, elementC, ...]

		let grades = [98.7, 92.1, 90.2, 94.6];//similar data types
		console.log(grades);//(4) [98.7, 92.1, 90.2, 94.6]

		let details = ["John", "Smith", 32, true];
		console.log(details);

		//Objects
			//Objects are another special kind of data type that's used to mimic real world objects/items
			//They are used to create complex data that contains pieces of information that are RELEVANT to each other
			//every individual of piece of info is called a property of the object
		//Syntax
			//let/const objectName = {
				//propertyA: value,
				//property B: value
			//}

		let person = {
			firstName: 'Albedo',
			lastName: 'Cardo',
			age: 35,
			isMarried: false,
			contact: ["09123456789", "0987654321"],
			address: {
				houseNumber: '248',
				city: 'Quezon City'
			}
		}

		console.log(person);

		let myGrades = {
			firstGrading: 98.7,
			secondGrading: 92.1,
			thirdGrading: 90.2,
			fourthGrading: 94.6
		}

		console.log(myGrades);
		console.log(typeof myGrades);//object


		//const anime = ['OP', 'OPM', "AOT"];
		//anime = ['KNY'];
		//console.log(anime);

		//OP[0], OPM[1]
		const anime = ['OP', 'OPM', "AOT"];
		anime[0] = 'KNY';
		console.log(anime);//['KNY', 'OPM', "AOT"]

		//Null
		//it is used to intentionally express the absence of a value in a variale declaration/initialization
		//null simply means that a data type was assigned to a variable but it does not hold any value/amount or is nullified

		let spouse = null;

		//using null compared to a 0 value and an empty string is much better for readability purposes
		//null is also considered as a data type of its own compared to 0 which is a data type of a number and single quotes which are a data type of a string

		let myNumber = 0;
		let myString = '';

		//Undefined

		//represents the state of a variable that has been declared but without an assigned value

		let fullName;
		console.log(fullName);//undefined

		// Undefined vs Null
		// One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value
		// null means that a variable was created and was assigned a value that does not hold any value/amount
		// Certain processes in programming would often return a "null" value when certain tasks results to nothing
		let varA = null;
		console.log(varA);

		// For undefined, this is normally caused by developers creating variables that have no value/data associated with them
		// This is when the value of a variable is still unknown
		let varB;
		console.log(varB);